<?php
/*
Plugin Name: HTML5 Video
Plugin URI: http://sputnikmobile.com/
Description: HTML5 Video plugin
Version: 1.2
Author: the Sputnik Mobile team
Author URI: http://sputnikmobile.com/
Bitbucket Plugin URI: https://bitbucket.org/fishtag/html5-video
Bitbucket Branch: master
*/

/*Some Set-up*/
define('HTML5_VIDEO_PLUGIN_DIR', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );

/*-------------------------------------------------------------------------------*/
/*   Register Custom Post Types
/*-------------------------------------------------------------------------------*/
add_action( 'init', 'html5vp_create_post_type' );
function html5vp_create_post_type() {
  register_post_type( 'video',
    array(
      'labels' => array(
        'name' => 'Videos',
        'singular_name' => 'Video',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Video',
        'edit' => 'Edit',
        'edit_item' => 'Edit Video',
        'new_item' => 'New Video',
        'view' => 'View',
        'view_item' => 'View Video',
        'search_items' => 'Search Videos',
        'not_found' => 'No Videos found',
        'not_found_in_trash' => 'No Videos found in Trash',
        'parent' => 'Parent Video'
      ),
      'public' => true,
      'show_ui' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => true,
      'has_archive' => false,
      'taxonomies'  => array( 'category' ),
      'hierarchical' => false,
      'menu_position' => 15,
      'supports' => array( 'title' ),
      'capability_type' => 'page',
      'rewrite' => array( 'slug' => 'video' ),
      'menu_icon' => 'dashicons-format-video'
    )
  );
}

/*-------------------------------------------------------------------------------*/
/*   CMB2
/*-------------------------------------------------------------------------------*/
//include_once('inc/cmb2/init.php');
include_once('inc/cmb2/fields.php');

/*-------------------------------------------------------------------------------*/
/*   Hide & Disabled View, Quick Edit and Preview Button
/*-------------------------------------------------------------------------------*/
function html5vp_remove_row_actions( $idtions ) {
  global $post;
  if( $post->post_type == 'video' ) {
    unset( $idtions['view'] );
    unset( $idtions['inline hide-if-no-js'] );
  }
  return $idtions;
}

if ( is_admin() ) {
  add_filter( 'post_row_actions','html5vp_remove_row_actions', 10, 2 );}

/*-------------------------------------------------------------------------------*/
/* HIDE everything in PUBLISH metabox except Move to Trash & PUBLISH button
/*-------------------------------------------------------------------------------*/

function html5vp_hide_publishing_actions(){
  $my_post_type = 'video';
  global $post;
  if($post->post_type == $my_post_type){
    echo '
                <style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                        display:none;
                    }
                </style>
            ';
  }
}
add_action('admin_head-post.php', 'html5vp_hide_publishing_actions');
add_action('admin_head-post-new.php', 'html5vp_hide_publishing_actions');

/*-------------------------------------------------------------------------------*/
/* code for change publish button to save.
/*-------------------------------------------------------------------------------*/

add_filter( 'gettext', 'html5vp_change_publish_button', 10, 2 );

function html5vp_change_publish_button( $translation, $text ) {
  if ( 'video' == get_post_type())
    if ( $text == 'Publish' )
      return 'Save';

  return $translation;
}

/*-------------------------------------------------------------------------------*/
/* Lets register our shortcode
/*-------------------------------------------------------------------------------*/
function html5vp_cpt_content_func($atts){
  extract( shortcode_atts( array(

    'id' => null,

  ), $atts ) );

  ?>
  <?php
    ob_start();
    $html5vp_title      = get_the_title($id);
    $html5vp_poster     = get_post_meta($id,'_ahp_video-poster', true);
    $html5vp_width      = get_post_meta($id,'_ahp_video-size', true);
    $html5vp_control    = get_post_meta($id,'_ahp_video-control', true);
    $html5vp_loop       = get_post_meta($id,'_ahp_video-repeat', true);
    $html5vp_muted      = get_post_meta($id,'_ahp_video-muted', true);
    $html5vp_autoplay   = get_post_meta($id,'_ahp_video-autoplay', true);
    $html5vp_categories = get_the_category($id);

  ?>
  <div class="html5vp">
    <video class="video-js vjs-custom-skin vjs-big-play-centered" preload="metadata" data-setup='{"fluid": true}'
      <?php echo ($html5vp_poster ? 'poster="' . $html5vp_poster . '"' : ''); ?>
      <?php echo ($html5vp_width ? 'width="' . $html5vp_width . '"' : ''); ?>
      <?php echo ($html5vp_control == 'on' ? '' : 'controls'); ?>
      <?php echo ($html5vp_loop == 'loop' ? 'loop' : ''); ?>
      <?php echo ($html5vp_muted == 'on' ? 'muted' : ''); ?>
      <?php echo ($html5vp_autoplay == 'on' ? 'autoplay' : ''); ?>
    >
      <source src="<?php echo get_post_meta($id,'_ahp_video-file', true); ?>">
      <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
    </video>
    <div class="html5vp__meta clearfix">
      <div class="html5vp__info">
        <?php if ( ! empty( $html5vp_categories ) ) :
          $separator = ', ';
          $output = ''; ?>
          <p class="html5vp__info__category">
            <?php foreach( $html5vp_categories as $category ) :
              $output .= esc_html( $category->name ) . $separator;
            endforeach; echo trim( $output, $separator ); ?>
          </p>
        <?php endif; ?>
        <p class="html5vp__info__title"><?php echo $html5vp_title; ?></p>
      </div>
      <?php if ( function_exists( 'get_simple_likes_button' ) ) : ?>
      <div class="html5vp__like">
        <?php echo get_simple_likes_button( $id ); ?>
      </div>
      <?php endif; ?>
    </div>
  </div>
  <?php
    $output = ob_get_clean();
    return $output; //print $output; // debug ?>
  <?php
}
add_shortcode('video','html5vp_cpt_content_func');

/*-------------------------------------------------------------------------------*/
/* TinyMce
/*-------------------------------------------------------------------------------*/
require_once( 'tinymce/html5vp-tinymce.php' );


// ONLY MOVIE CUSTOM TYPE POSTS
add_filter('manage_video_posts_columns', 'ST4_columns_head_only_video', 10);
add_action('manage_video_posts_custom_column', 'ST4_columns_content_only_video', 10, 2);

// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function ST4_columns_head_only_video($defaults) {
  $defaults['directors_name'] = 'ShortCode';
  return $defaults;
}
function ST4_columns_content_only_video($column_name, $post_ID) {
  if ($column_name == 'directors_name') {
    // show content of 'directors_name' column
    echo '<input value="[video id='. $post_ID . ']" >';
  }
}