<?php
add_action( 'cmb2_admin_init', 'html5vp_yourprefix_register_demo_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function html5vp_yourprefix_register_demo_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_ahp_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox2',
		'title'         => __( 'Configure your video player' ),
		'object_types'  => array( 'video', )
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Upload Video', 'cmb2' ),
		'desc' => __( 'Upload an Mp4 or Ogg file. if you select an other file type it will not play', 'cmb2' ),
		'id'   => $prefix . 'video-file',
		'type' => 'file',
	) );
	
	$cmb_demo->add_field( array(
		'name' => __( 'Poster Image', 'cmb2' ),
		'desc' => __( 'Specifies an image to be shown while the video is downloading, or until the user hits the play button', 'cmb2' ),
		'id'   => $prefix . 'video-poster',
		'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name'             => __( 'Repeat', 'cmb2' ),
		'desc'             => __( '', 'cmb2' ),
		'id'               => $prefix . 'video-repeat',
		'type'             => 'radio_inline',
		'options'          => array(
			'once' => __( 'Repeat Once', '' ),
			'loop'   => __( 'Repeat again and again', 'loop' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Muted', 'cmb2' ),
		'desc' => __( 'Check if you want the video output should be muted', 'cmb2' ),
		'id'   => $prefix . 'video-muted',
		'type' => 'checkbox',
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Hide Control', 'cmb2' ),
		'desc' => __( 'Check if you want to hide the video control  (such as a play/pause button etc).', 'cmb2' ),
		'id'   => $prefix . 'video-control',
		'type' => 'checkbox',
	) );	

	$cmb_demo->add_field( array(
		'name' => __( 'Auto Play', 'cmb2' ),
		'desc' => __( 'Check if you want video will start playing as soon as it is ready', 'cmb2' ),
		'id'   => $prefix . 'video-autoplay',
		'type' => 'checkbox',
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Player Width In pixel ', 'cmb2' ),
		'desc' => __( 'Sets the player width. Height will be calculate base on the value.Left blank for default value', 'cmb2' ),
		'id'   => $prefix . 'video-size',
		'type' => 'text_small',
	) );
}
