<?php
/*-------------------------------------------------------------------------------*/
/*   AJAX Get Slider List
/*-------------------------------------------------------------------------------*/
function html5vp_grab_slider_list_ajax() {
	
	if ( !isset( $_POST['grabslider'] ) ) {
		wp_die();
		} 
		else {
			
			$list = array();
			
			global $post;
			
			$args = array(
  				'post_type' => 'video',
  				'order' => 'ASC',
				'posts_per_page' => -1,
  				'post_status' => 'publish'
		
				);

				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :	setup_postdata($post);

				$list[$post->ID] = array('val' => $post->ID, 'title' => esc_html(esc_js(the_title(NULL, NULL, FALSE))) );

				endforeach;
				
				}
		
			echo json_encode($list); //Send to Option List ( Array )
			wp_die();


	}

add_action('wp_ajax_html5vp_grab_slider_list_ajax', 'html5vp_grab_slider_list_ajax');

/*-------------------------------------------------------------------------------*/
/*   Frontend Register JS & CSS
/*-------------------------------------------------------------------------------*/
function html5vp_reg_script() {
	wp_register_style( 'html5vp-tinymcecss', plugins_url( 'tinymce/tinymce.css' , dirname(__FILE__) ), false, 'all');
	wp_register_script( 'html5vp-tinymcejs', plugins_url( 'tinymce/tinymce.js' , dirname(__FILE__) ), false );
}
add_action( 'admin_init', 'html5vp_reg_script' );


//-------------------------------------------------------------------------------------------------	
if ( strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post-new.php' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post.php' ) ) {
	
// ADD STYLE & SCRIPT
	add_action( 'admin_head', 'html5vp_editor_add_init' );
	function html5vp_editor_add_init() {

		if ( get_post_type( get_the_ID() ) != 'video' ) {

			wp_enqueue_style( 'html5vp-tinymcecss' );
			wp_enqueue_script( 'html5vp-tinymcejs' );

	?>
			<?php
		}
	}
	
// ADD MEDIA BUTOON	
	add_action( 'media_buttons_context', 'html5vp_shortcode_button', 1 );
	function html5vp_shortcode_button($context) {
		$img = HTML5_VIDEO_PLUGIN_DIR .'img/play.png';
		$container_id = 'html5vpmodal';
		$title = 'Insert Html5 Video';
		$context .= '
		<a class="thickbox button" id="html5vp_shortcode_button" title="'.$title.'" style="outline: medium none !important; cursor: pointer;" >
		<img src="'.$img.'" alt="" width="20" height="20" style="position:relative; top:-1px"/>Html5 video</a>';
		return $context;
	}
}


// GENERATE POPUP CONTENT
add_action('admin_footer', 'html5vp_popup_content');
function html5vp_popup_content() {

if ( strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post-new.php' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post.php' ) ) {

if ( get_post_type( get_the_ID() ) != 'video' ) {
// START GENERATE POPUP CONTENT

?>
<div id="html5vpmodal" style="display:none;">
<div id="tinyform" style="width: 550px;">
<form method="post">

<div class="html5vp_input" id="html5vptinymce_select_slider_div">
<label class="label_option" for="html5vptinymce_select_slider">Html5 Video</label>
	<select class="html5vp_select" name="html5vptinymce_select_slider" id="html5vptinymce_select_slider">
    <option id="selectshtml5vplider" type="text" value="select">- Select Player -</option>
</select>
<div class="clearfix"></div>
</div>

<div class="html5vp_button">
<input type="button" value="Insert Shortcode" name="html5vp_insert_scrt" id="html5vp_insert_scrt" class="button-secondary" />
<div class="clearfix"></div>
</div>

</form>
</div>
</div>
<?php 
	}
  } //END
}

?>